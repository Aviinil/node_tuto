var rect = {
    perimeter: (x,y)=>2*(x+y),
    area: (x,y)=> x*y,
};

//exports.perimeter = rect.perimeter;
//exports.area = rect.area;
//autre maniere
module.exports = rect;
