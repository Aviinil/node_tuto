var rect = require("./rectangle");
function solveRect(l,b) {
    //l : longueur
    //b : largeur
    console.log("Solving for rectangle with l = " + l + "and b = "+b);
    if (l<= 0 || b <=0) {
        console.log("Les deux dimensions doivent être supérieures à 0");
        console.log("-----");
    } else {
        console.log("The area of rectangle is: "+ rect.area(l,b));
        console.log("The perimeter of rectangle is: "+ rect.perimeter(l,b));
        console.log("-----");
    }
};

solveRect(2,2);
solveRect(2,-2);
